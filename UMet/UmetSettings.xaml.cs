﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UMet.Classes;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace UMet
{
	public sealed partial class UmetSettings : UserControl
	{
		public UmetSettings()
		{
			this.InitializeComponent();
			txtUsername.Text = Umet8Storage.Username;
			txtPassword.Password = Umet8Storage.Password;
			cboServices.Items.Add(Umet8Storage.Service);
			cboServices.SelectedIndex = 0;
		}

		private void GoBack(object sender, RoutedEventArgs e)
		{
			if (this.Parent.GetType() == typeof(Popup))
			{
				((Popup)this.Parent).IsOpen = false;
			}
			SettingsPane.Show();
		}

		private async void btnRetrieve_Click(object sender, RoutedEventArgs e)
		{
			cboServices.Items.Clear();
			  cboServices.Items.Add("Loading, please wait...");
			  cboServices.SelectedIndex = 0;
			  
			  var net = new Internode();
			  net.Username = txtUsername.Text;
			  net.Password = txtPassword.Password;

			  cboServices.Items.Clear();

			  try
			  {
				  var serviceList = await net.RequestServices();
			  
				  if (serviceList.Count() > 0)
				  {

					  foreach (var service in serviceList)
					  {
						  cboServices.Items.Add(service.Id + ": " + service.Type);
					  }
				  }
				  else
				  {
					  cboServices.Items.Add("No services found");
				  }
			  }
			  catch (IOException ex)
			  {
				  cboServices.Items.Add("Error: " + ex.Message);
			  }

			  cboServices.SelectedIndex = 0;
		  }

		private void btnSave_Click(object sender, RoutedEventArgs e)
		{
			Umet8Storage.Username = txtUsername.Text;
			Umet8Storage.Password = txtPassword.Password;
			Umet8Storage.Service = cboServices.SelectedValue.ToString();
		}
	}
}