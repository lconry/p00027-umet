﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using UMet.Classes;
using Windows.UI.ApplicationSettings;
using Windows.System;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace UMet
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {

		 Rect _windowBounds;
		 double _settingsWidth = 346;
		 Popup _settingsPopup;

		  /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;

				
				
        }

		 public void App_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
   {
		SettingsCommand cmd = new SettingsCommand("account", "Acount Settings", (x) =>
		{
			_settingsPopup = new Popup();
			_settingsPopup.Closed += OnPopupClosed;
			Window.Current.Activated += OnWindowActivated;
			_settingsPopup.IsLightDismissEnabled = true;
			_settingsPopup.Width = _settingsWidth;
			_settingsPopup.Height = _windowBounds.Height;

			var mypane = new UmetSettings();
			mypane.Width = _settingsWidth;
			mypane.Height = _windowBounds.Height;

			_settingsPopup.Child = mypane;
			_settingsPopup.SetValue(Canvas.LeftProperty, _windowBounds.Width - _settingsWidth);
			_settingsPopup.SetValue(Canvas.TopProperty, 0);
			_settingsPopup.IsOpen = true;
		});

		args.Request.ApplicationCommands.Add(cmd);
    }

		 void OnWindowSizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
		 {
			 _windowBounds = Window.Current.Bounds;
		 }

        
        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            Frame rootFrame = Window.Current.Content as Frame;

				_windowBounds = Window.Current.Bounds;

				Window.Current.SizeChanged += OnWindowSizeChanged;

				SettingsPane.GetForCurrentView().CommandsRequested += App_CommandsRequested;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();

				
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

		  private void OnWindowActivated(object sender, Windows.UI.Core.WindowActivatedEventArgs e)
		  {
			  if (e.WindowActivationState == Windows.UI.Core.CoreWindowActivationState.Deactivated)
			  {
				  _settingsPopup.IsOpen = false;
			  }
		  }

		  void OnPopupClosed(object sender, object e)
		  {
			  Window.Current.Activated -= OnWindowActivated;
		  }

		  private void Button_Click_1(object sender, RoutedEventArgs e)
		  {
			  SettingsPane.Show();
		  }

		  private void HandleSizeChange(object sender, RoutedEventArgs e)
		  {
			  RadioButton rb = sender as RadioButton;
			  _settingsWidth = Convert.ToInt32(rb.Content);
		  }
    }
}
