﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace UMet.Classes
{
	class Internode
	{
		const string ApiUrl = @"https://customer-webtools-api.internode.on.net/api/v1.5/";
		public string Service { get; set; }
		public string Name { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string UserAgent { get; set; }
		public string Usage { get; set; }

		public async Task<XDocument> RequestData( string url )

		{
			
			var authHeader = new AuthenticationHeaderValue(
			  "Basic",
			  System.Convert.ToBase64String( System.Text.UTF8Encoding.UTF8.GetBytes(
				string.Format("{0}:{1}", this.Username, this.Password))));

			var content = new StringContent("");
			content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
			var client = new System.Net.Http.HttpClient();
			client.DefaultRequestHeaders.Authorization = authHeader;
			client.DefaultRequestHeaders.Add("user-agent", "NetUsage/8.0.0.dev (Windows 8)");


			var response = await client.PostAsync(url, content);
			 if (response.IsSuccessStatusCode)
			 {
				 return XDocument.Load( await response.Content.ReadAsStreamAsync() );
				 
			 }
			 else
			 {
				 throw new IOException(response.StatusCode.ToString());
			 }

			}

		public async Task<IEnumerable<dynamic>> RequestServices()
		{

			XDocument xDoc = await RequestData(ApiUrl);
			var list = (from service in xDoc.Descendants("service")
							select new {
								Id= service.Value,
								Type=service.Attribute("type").Value
							}).ToList();
			return list;


		}
	}
}
