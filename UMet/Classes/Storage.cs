﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace UMet.Classes
{
	class Umet8Storage
	{
		public static ApplicationDataContainer SettingsContainer { get; set; }

		static Umet8Storage()
		{
			 SettingsContainer = ApplicationData.Current.RoamingSettings;
		}

		public static string Username
		{
			 get { return (SettingsContainer.Values["Username"] != null ? SettingsContainer.Values["Username"] : "") as string; }
			set { SettingsContainer.Values["Username"] = value; }
		}

		public static string Password
		{
			get { return (SettingsContainer.Values["Password"] != null ? SettingsContainer.Values["Password"] : "") as string; }
			set { SettingsContainer.Values["Password"] = value; }
		}

		public static string Service
		{
			get { return (SettingsContainer.Values["Service"] != null ? SettingsContainer.Values["Service"] : "None Retrieved") as string; }
			set { SettingsContainer.Values["Service"] = value; }
		}
	}
}
